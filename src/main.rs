use regex::Regex;
use std::io;
fn match_for_unit(c: &str) -> f32 {
    match c {
        "K" => return 1000.0,
        "H" => return 100.0,
        "D" => return 10.0,
        "u" => return 1.0,
        "d" => return 0.1,
        "c" => return 0.01,
        "m" => return 0.001,
        _ => panic!("the Unit '{}' doesnt matches", c),
    }
}

fn main() {
    let mut input = String::new();
    println!("You need to enter a string like 12.0_u_K_1\nwhere 12 is the number, u is the unit of the number\nK is the unit to which you want to pass and 1 is the base.");
    let re = Regex::new(r"(\d*\.\d*)_(\D{1})_(\D{1})_(\d{1})").unwrap();
    io::stdin()
        .read_line(&mut input)
        .expect("Failed to read input");
    let mut number: f32;
    let mut base: i32;
    let mut result: f32;
    for cap in re.captures_iter(input.as_str()) {
        println!(
            "Number {} unit1 {} unit2 {} base {}",
            &cap[1], &cap[2], &cap[3], &cap[4]
        );
        number = cap[1].parse::<f32>().unwrap();
        base = cap[4].parse::<i32>().unwrap();
        result = match_for_unit(&cap[2]) / match_for_unit(&cap[3]).powi(base) * number;
        println!("The Result is {}!", result);
    }
}
